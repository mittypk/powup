import struct
import hashlib
import socket
import binascii
import time
import logging
from concurrent.futures import ThreadPoolExecutor
import threading
import sys
import argparse

# SERVER CONSTANTS
# Max powup payload size (should leave at 65487/max for virtual networks, compatible with smaller sizes)
CHUNK_SIZE = 65487
# CPU threads to use
SERVER_CPU_THREADS = 8
# Buffer size (default is 50MB)
SERVER_BUFFER_SIZE = 52428800
# Interval between garbage collection (default is 5 seconds)
SERVER_GC_INTERVAL = 5
# Time after the last packet before connection is timed out and removed
SERVER_TIMEOUT = 1
# Logging level (DEBUG, INFO, ERROR)
LOG_LEVEL = logging.DEBUG

class PowupServer:
    # constructor
    def __init__(self, ip_addr, port):
        self.ip_addr = ip_addr
        self.port = port
        self.sock = None
        self.active_conns = {}
        self.timeout_timers = {}
        self.speed_timers = {}
        self.lock = threading.Lock()
        self.executor = ThreadPoolExecutor(max_workers=SERVER_CPU_THREADS)
        self.quit = False
        self.quitNotifier = threading.Event()
        self.chunkCounters = {}
        self.byteCounters = {}
        self.completed = {}

    def __verify_powup_fields(self, powup_data):
        # verify the connection ID is positive
        if powup_data["conn_id"] <= 0:
            logging.error("ID field was non-positive.")
            logging.error(f"Connection ID: {powup_data['conn_id']}")
            return False
        # check size is positve
        elif powup_data["size"] <= 0:
            logging.error("Size field was non-positive.")
            logging.error(f"Size: {powup_data['size']}")
            return False
        # verify chunk count is positive
        elif powup_data["chunk_count"] <= 0:
            logging.error("Chunk count field was non-positive.")
            logging.error(f"Chunk count: {powup_data['chunk_count']}")
            return False
        # check chunk size is positive and indicates size of payload
        elif powup_data["chunk_size"] <= 0 or (powup_data["chunk_size"] != len(powup_data["payload"])):
            logging.error("Error. Chunk size field was either non-positive or didn't match the payload size.")
            logging.error(f"Chunk size: {powup_data['chunk_size']} payload length: {len(powup_data['payload'])}")
            return False
        # check chunk ID is not negative or greater than the chunk count
        elif (powup_data["chunk_id"] < 0) or (powup_data["chunk_id"] > powup_data["chunk_count"]):
            logging.error("Error. Chunk ID field was invalid.")
            logging.error(f"Chunk ID: {powup_data['chunk_id']}")
            return False
        else:
            return True

    def __process_powup_packet(self, packet):
        # extract sender and udp data
        udp_data = packet[0]
        sender = packet[1]

        # check the size of the udp data
        size = len(udp_data)
        # if less than 20 it is not a valid powup packet
        if size <= 20 or size > 65507:
            logging.error("Dropping Packet. Packet size was not within acceptable range.")
            logging.error(f"Packet Size: {str(size)} Sender: {sender}")

        # define the C type format string
        format_string = struct.Struct("I I I I I " + str(size - 20) + "s")

        # try unpack the powup packet
        try:
            powup_packet = format_string.unpack(udp_data)
        except struct.error as err:
            logging.error("Dropping packet. Unable to unpack data to powup structure.")
            logging.error(f"Sender: {sender}")
            logging.error(err)
            return

        # convert to dict
        powup_data = {
            "conn_id": powup_packet[0],
            "size": powup_packet[1],
            "chunk_count": powup_packet[2],
            "chunk_size": powup_packet[3],
            "chunk_id": powup_packet[4],
            "payload": powup_packet[5],
        }

        # verify packet fields
        if not (self.__verify_powup_fields(powup_data)):
            logging.error("Dropping packet. One or more fields did not conform to powup specification.")
            logging.error(f"Sender: {sender}")

        # acquire the lock
        self.lock.acquire()
        logging.debug(f"Lock acquired by Thread: {threading.get_ident()} ID: {powup_data['conn_id']} "
                      f"Chunk ID: {powup_data['chunk_id']}")

        # check if the connection already exists
        if powup_data["conn_id"] in self.active_conns:
            # ensure this isn't a duplicate
            if self.active_conns[powup_data["conn_id"]][powup_data["chunk_id"]] is not None:
                logging.debug("Duplicate packet received.")
                logging.debug(f"ID: {powup_data['conn_id']} Chunk: {powup_data['chunk_id']}")
            # else store the packet
            else:
                logging.debug(f"Storing packet ID: {powup_data['conn_id']} Chunk ID: {powup_data['chunk_id']}")
                self.active_conns[powup_data["conn_id"]][powup_data["chunk_id"]] = powup_data["payload"]
                # update the counters
                self.chunkCounters[powup_data["conn_id"]] += 1
                self.byteCounters[powup_data["conn_id"]] += powup_data["chunk_size"]
                # reset timer
                self.timeout_timers[powup_data["conn_id"]] = time.time()
                # check if we have all chunks
                logging.debug(f"ID: {powup_data['conn_id']} "
                              f"Received: {self.chunkCounters[powup_data['conn_id']]} of "
                              f"{len(self.active_conns[powup_data['conn_id']])}")
                if self.chunkCounters[powup_data["conn_id"]] == len(self.active_conns[powup_data["conn_id"]]):
                    # call the fun function
                    self.__fin_connection(powup_data["conn_id"])
        # else new connection
        else:
            # Print/log new connection message to terminal
            logging.info(f"------ New Connection ------")
            logging.info(f"ID: {powup_data['conn_id']} Size: {powup_data['size']} "
                         f"Chunks: {powup_data['chunk_count']} Sender: {sender[0]}")

            # create an empty connection list
            self.active_conns[powup_data["conn_id"]] = [None] * (powup_data["chunk_count"] + 1)
            # store packet
            logging.debug(f"Storing packet ID: {powup_data['conn_id']} Chunk ID: {powup_data['chunk_id']}")
            self.active_conns[powup_data["conn_id"]][powup_data["chunk_id"]] = powup_data["payload"]
            # store the time
            self.timeout_timers[powup_data["conn_id"]] = self.speed_timers[powup_data["conn_id"]] = time.time()
            # create chunk counter for connection
            self.chunkCounters[powup_data["conn_id"]] = 1
            # init the byte counter
            self.byteCounters[powup_data["conn_id"]] = powup_data["chunk_size"]
            # set to not complete
            self.completed[powup_data["conn_id"]] = False

        # release the lock
        self.lock.release()
        logging.debug(f"Lock released by Thread: {threading.get_ident()} ID: {powup_data['conn_id']} "
                      f"Chunk ID: {powup_data['chunk_id']}")

    # function to finish a connection once all chunks are received
    def __fin_connection(self, conn_id):
        # calculate time and speed
        tx_time = time.time() - self.speed_timers[conn_id]
        tx_speed = (self.byteCounters[conn_id] / (1024*1024)) / tx_time
        # calculate the hash
        tx_hash = hashlib.sha256()
        for index in range(1, len(self.active_conns[conn_id])):
            tx_hash.update(self.active_conns[conn_id][index])
        # check hashes and write our file
        if self.active_conns[conn_id][0] == tx_hash.digest():
            logging.info(f"ID: {conn_id} transfer successful.")
            logging.debug(f"Hash: {binascii.hexlify(tx_hash.digest())}")
            logging.info(f"Time: {tx_time:.2f} s Avg speed: {tx_speed:.2f} MB/s")
            # write out the file as the SHA256 hash
            self.__write_to_file(conn_id)
            # add to completed
            self.completed[conn_id] = True
        # failed connection
        else:
            logging.error(f"ID: {conn_id} transfer failed. Hashes did not match.")
            logging.error(f"FIN       : {binascii.hexlify(self.active_conns[conn_id][0])}")
            logging.error(f"Calculated:  {binascii.hexlify(tx_hash.digest())}")

    # function to remove a connection from all internal members
    def __rem_connection(self, conn_id):
        # remove connection data
        del self.active_conns[conn_id]
        del self.timeout_timers[conn_id]
        del self.speed_timers[conn_id]
        del self.chunkCounters[conn_id]
        del self.completed[conn_id]
        del self.byteCounters[conn_id]

    # function used to cleanup expired connections
    def __start_garbage_collect(self):
        while 1 and not self.quit:
            logging.debug("Garbage collector started.")
            # make a list of connections to remove
            garbage = []
            # acquire lock
            self.lock.acquire()

            # loop through connection timers to find expired
            for conn_id, timer in self.timeout_timers.items():
                current_time = time.time()
                if int(current_time - timer) > SERVER_TIMEOUT:
                    # add to remove list
                    garbage.append(conn_id)

            # remove connections from list
            for expired_conn in garbage:
                if self.completed[conn_id] is True:
                    self.__rem_connection(expired_conn)
                else:
                    self.__rem_connection(expired_conn)
                    # log and print connection removal
                    logging.info(f"ID: {expired_conn} timed out and was removed.")

            # print active connections
            logging.debug(f"Active connections: {len(self.active_conns)}")
            # release the lock
            self.lock.release()
            # sleep
            self.quitNotifier.wait(SERVER_GC_INTERVAL)

    # function that writes a given connection to file
    def __write_to_file(self, conn_id):
        # generate unique filename
        filename = binascii.hexlify(self.active_conns[conn_id][0])
        # open file handle
        file = open(filename, "wb")
        # loop and write out the bytes
        for index in range(1, len(self.active_conns[conn_id])):
            file.write(self.active_conns[conn_id][index])
        # close file handle
        file.close()
        # log
        logging.info(f"ID: {conn_id} File saved as {filename}")

    # starts the server daemon
    def start(self):
        ascii = """
           ___  ____ _      ____  _____    ___________ _   _________ 
          / _ \/ __ \ | /| / / / / / _ \  / __/ __/ _ \ | / / __/ _ \\
         / ___/ /_/ / |/ |/ / /_/ / ___/ _\ \/ _// , _/ |/ / _// , _/
        /_/   \____/|__/|__/\____/_/    /___/___/_/|_||___/___/_/|_|
        Potters' One-Way UDP Protocol Server
        Version 1.0 
        """
        print(ascii)
        # create a logger
        format = '[%(asctime)s] %(levelname)s:%(message)s'
        dformat = '%d/%m/%Y %H:%M:%S'
        logging.basicConfig(level=LOG_LEVEL, format=format, datefmt=dformat)
        logger = logging.getLogger()
        # create a log file
        handler = logging.FileHandler('powup-server.log')
        # create a logging format
        formatter = logging.Formatter(format)
        handler.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(handler)

        # open the socket
        logging.info(f"Running server on {self.ip_addr}:{self.port}")
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # try to set the buffer size to 50MB
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SERVER_BUFFER_SIZE)
        # check system buffer settings
        if self.sock.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF) < (SERVER_BUFFER_SIZE*2):
            print("UDP buffer is too small, please enter the following command to increase it to 50MB:")
            print(f"sudo sysctl -w net.core.rmem_max={str(SERVER_BUFFER_SIZE)}")
            self.sock.close()
            return

        # bind to the socket
        try:
            self.sock.bind((self.ip_addr, self.port))
        except Exception:
            logging.error(f"Could not start server on {self.ip_addr}:{self.port}. Exiting...")
            sys.exit()

        # launch the garbage collector
        self.executor.submit(self.__start_garbage_collect)

        try:
            print(f"Ready, Listening...\n")
            # main producer loop
            while 1 and not self.quit:
                # grab a packet
                udp_data, sender = self.sock.recvfrom(CHUNK_SIZE+20)
                # process the packet
                self.executor.submit(self.__process_powup_packet, (udp_data, sender))
        # exit on ctrl+c
        except KeyboardInterrupt:
            print("\nClosing server...")
            # close the socket
            self.sock.close()
            # signal threads to die
            self.quit = True
            # wait until all threads close
            print("Closing all threads...")
            # wake up to garbage collector
            self.quitNotifier.set()
            # close the thread pool
            self.executor.shutdown()
            logging.info("Server stopped.")
            # exit
            sys.exit()


# run server as a program
if __name__ == '__main__':
    # Define arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--ip", default="0.0.0.0", help="IP address to run the server on (default 0.0.0.0)", type=str)
    parser.add_argument("-p", "--port", default=7777, help="Port to run the server on (default 7777)", type=int)
    args = parser.parse_args()

    # check whether to run server or client
    server = PowupServer(args.ip, args.port)
    server.start()

