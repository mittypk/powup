import time
import sys
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class Watcher:
    DIRECTORY_TO_WATCH = sys.argv[1]

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print "Error"

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):

        import powupclient

        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print "New Malware: %s." % event.src_path
            client = powupclient.PowupClient("103.57.0.185",7777)
            client.send_file(event.src_path)
       # elif event.event_type == 'modified':
       #     # Taken any action here when a file is modified.
       #     print "Received modified event - %s." % event.src_path


if __name__ == '__main__':
    w = Watcher()
    w.run()