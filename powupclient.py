import struct
import uuid
import os
import hashlib
import socket
import time
import argparse

# Constants
# Max powup payload size
CHUNK_SIZE = 65487
# Time to sleep between UDP packets to throttle speed (should be zero for virtual networks)
SLEEP_INTERVAL = 0


class PowupClient:
    # constructor
    def __init__(self, ip_addr, port):
        self.port = port
        self.ip_addr = ip_addr
        self.hash = ""

    # function that takes header data and payload to construct a powup packet
    def __new_powup_packet(self, powup_data):
        # make the python tuple for the powup packet
        packet = (powup_data["conn_id"], powup_data["size"], powup_data["chunk_count"],
                  powup_data["chunk_size"], powup_data["chunk_id"], powup_data["payload"])
        # define the C type format string
        format_string = struct.Struct("I I I I I " + str(len(powup_data["payload"])) + "s")
        # convert the packet into a C struct
        powup_packet = format_string.pack(*packet)
        return powup_packet

    # method to send a file using the client
    def send_file(self, file_path):

        # check file exists
        if not os.path.isfile(file_path):
            print("Error. file could not be found, check the file path is correct.")
            return

        # clear the hash
        self.hash = hashlib.sha256()
        # get a handle to the file
        file_handle = open(file_path, "rb")
        # generate a connection id
        conn_id = uuid.uuid4().int & (1 << 32) - 1
        print("Connection ID: "+str(conn_id))
        # get size of the file
        size = os.stat(file_path).st_size
        # determine the total number of chunks
        remainder = size % CHUNK_SIZE
        chunk_count = int(size / CHUNK_SIZE)
        if remainder > 0:
            chunk_count += 1
        # start id at 1
        chunk_id = 1
        print("Sending " + str(size) + " bytes over "+ str(chunk_count) + " chunks...")
        # generate packets and send them
        while chunk_id <= chunk_count:
            # generate the payload
            payload = file_handle.read(CHUNK_SIZE)
            # update the hash
            self.hash.update(payload)
            # add the packet data to a dictionary
            powup_data = {
                "conn_id": conn_id,
                "size": size,
                "chunk_count": chunk_count,
                "chunk_size": len(payload),
                "chunk_id": chunk_id,
                "payload": payload,
            }
            # make the packet
            powup_packet = self.__new_powup_packet(powup_data)
            # send the packet
            self.__send_powup_packet(powup_packet)
            # increment chunk id
            chunk_id += 1

        print("SHA256 hash: " + self.hash.hexdigest())
        # create the fin packet
        powup_data = {
            "conn_id": conn_id,
            "size": size,
            "chunk_count": chunk_count,
            "chunk_size": len(self.hash.digest()),
            "chunk_id": 0,
            "payload": self.hash.digest(),
        }
        # make the packet
        powup_packet = self.__new_powup_packet(powup_data)
        # send the fin packet
        self.__send_powup_packet(powup_packet)
        print("FIN packet sent. Done.")

    def __send_powup_packet(self, powup_packet):
        # create udp socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # send udp packet
        sock.sendto(powup_packet, (self.ip_addr, self.port))
        # sleep to throttle speed
        time.sleep(SLEEP_INTERVAL)


# run client as a program
if __name__ == '__main__':
    # Define arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--ip", default="0.0.0.0", help="IP address to send too (default 0.0.0.0)", type=str)
    parser.add_argument("-p", "--port", default=7777, help="Port to use (default 7777)", type=int)
    parser.add_argument("file", help="Full path of file to send (Must be full path!)", type=str)
    args = parser.parse_args()

    # check whether to run server or client
    client = PowupClient(args.ip, args.port)
    client.send_file(args.file)
