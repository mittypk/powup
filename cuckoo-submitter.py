import os
import time
import sys
import logging
import requests
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# Constants
REST_URL = "http://localhost:8090/tasks/create/file"
HEADERS = {"Authorization": "Bearer cuckoo-submitter"}
SLEEP_TIME = 1
# Logging level (DEBUG, INFO, ERROR)
LOG_LEVEL = logging.INFO

class Watcher:
    DIRECTORY_TO_WATCH = sys.argv[1]

    def __init__(self):
        self.observer = Observer()

    def run(self):
        # create a logger
        format = '[%(asctime)s] %(levelname)s:%(message)s'
        dformat = '%d/%m/%Y %H:%M:%S'
        logging.basicConfig(level=LOG_LEVEL, format=format, datefmt=dformat)
        logger = logging.getLogger()
        # create a log file
        handler = logging.FileHandler('cuckoo-submitter.log')
        # create a logging format
        formatter = logging.Formatter(format)
        handler.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(handler)

        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                logging.debug(f"Sleeping for {SLEEP_TIME} seconds...")
                time.sleep(SLEEP_TIME)
                logging.debug("Woke up...")
        # close on ctrl+c
        except KeyboardInterrupt:
            print("CTRL+C detected, exiting....")
            self.observer.stop()
            logging.info(f"Server exited.")

        self.observer.join()


# Handler for any file system event
class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        # Check if it was a directory event
        if event.is_directory:
            return None
        # else it was a file, ensure it was a creation event
        elif event.event_type == 'created':
            # Print file name
            logging.info(f"New File: {os.path.basename(event.src_path)}")
            # Open new file in read mode
            with open(event.src_path, "rb") as sample:
                files = {"file": (event.src_path, sample),
                         "platform": "windows"}
                data = {"options":"route=inetsim,procmemdump=yes"}
                # attempt connection
                try:
                    r = requests.post(REST_URL, headers=HEADERS, files=files, data=data)
                except requests.exceptions.ConnectionError:
                    logging.error("Could not make connection to Cuckoo server.")
                    return
                # Add your code to error checking for r.status_code.
                if r.status_code == 200:
                    logging.info(f"{os.path.basename(event.src_path)} was submitted to cuckoo.")
                    # get task id
                    task_id = r.json()["task_id"]
                    # check for invalid id
                    if task_id is None:
                        logging.error("Server return task ID of 'None'.")
                    else:
                        logging.info(f"Task id: {task_id}")
                elif r.status_code == 400:
                    logging.error("Server detected duplicate submission.")
                else:
                    logging.error("Unexpected HTTP code returned by server.")


if __name__ == '__main__':
    w = Watcher()
    w.run()
